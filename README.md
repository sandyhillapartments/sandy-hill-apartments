Sandy Hill Apartments – The latest in luxurious studio apartments, finally offered to students and young professionals in one of Ottawa's most sought after neighborhoods, Sandy Hill. Ottawa all-inclusive apartments are semi-furnished studio apartments, complete with high end finishes, are rented on an all-inclusive rent basis with a la carte services, such as VRTUCAR. They offer a one-of-a-kind Ottawa all-inclusive apartment experience that is conveniently located, safe, secure, of the highest quality - and excellent value!

Address: 353 Friel Street, Ottawa, ON K1N 7W7, Canada

Phone: 613-518-1929
